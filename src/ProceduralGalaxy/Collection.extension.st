Extension { #name : #Collection }

{ #category : #'*ProceduralGalaxy' }
Collection >> max: aSelectorOrOneArgBlock [
	^ (self collect: aSelectorOrOneArgBlock) max
]
