Class {
	#name : #PgxApplication,
	#superclass : #SgaApplication,
	#category : #'ProceduralGalaxy-Application'
}

{ #category : #activation }
PgxApplication class >> applicationName [

	^ 'ProceduralGalaxy'
]

{ #category : #initialization }
PgxApplication >> initialize [

	super initialize.
	self useBackend: #Gtk with: PgxGtkConfiguration new
]

{ #category : #accessing }
PgxApplication >> start [

	(PgxGalaxyViewPresenter newApplication: self) open
]
