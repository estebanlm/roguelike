Class {
	#name : #PgxPlanet,
	#superclass : #Object,
	#instVars : [
		'type',
		'diameter',
		'moons',
		'color'
	],
	#classVars : [
		'PlanetTypes'
	],
	#category : #'ProceduralGalaxy-Model'
}

{ #category : #'as yet unclassified' }
PgxPlanet class >> planetTypes [

	^ PlanetTypes ifNil: [ PlanetTypes := PgxPlanetType all ]
]

{ #category : #accessing }
PgxPlanet >> color [

	^ color
]

{ #category : #accessing }
PgxPlanet >> diameter [

	^ diameter
]

{ #category : #accessing }
PgxPlanet >> diameter: anObject [

	diameter := anObject
]

{ #category : #generating }
PgxPlanet >> generateWith: aRandom [
	| moonsPresence |	

	type := aRandom nextCollection: self class planetTypes.
	diameter := aRandom nextCollection: type typicalDiameter.
	color := aRandom nextCollection: type typicalColors.
	
	moonsPresence :=  aRandom nextCollection: type moonsPresence.
	moonsPresence > 0 ifTrue: [ 
		moons := Array new: moonsPresence.
		1 to: moonsPresence do: [ :index |
			moons at: index put: (PgxMoon new 
				generateWith: aRandom;
				yourself) ] ]
]

{ #category : #accessing }
PgxPlanet >> moons [

	^ moons ifNil: [ #() ]
]

{ #category : #accessing }
PgxPlanet >> numMoons [

	^ self moons size
]

{ #category : #accessing }
PgxPlanet >> radius [

	^ self diameter / 2.0
]

{ #category : #accessing }
PgxPlanet >> type [

	^ type
]

{ #category : #accessing }
PgxPlanet >> type: anObject [

	type := anObject
]
