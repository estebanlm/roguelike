Class {
	#name : #PgxStar,
	#superclass : #Object,
	#instVars : [
		'position',
		'type',
		'color',
		'hasStar',
		'diameter',
		'spectralType',
		'planets'
	],
	#classVars : [
		'Colors',
		'StarTypes'
	],
	#category : #'ProceduralGalaxy-Model'
}

{ #category : #accessing }
PgxStar class >> colors [

	^ Colors ifNil: [ 
		Colors := #(
			'9bb2ff' 
			'ccd8ff' 
			'eeefff' 
			'fff3ea' 
			'ffe8ce'
			'ffddb4' 
			'ffa94b'
			'ff5200' 
		) collect: [ :each | Color fromHexString: each ] ]
]

{ #category : #'instance creation' }
PgxStar class >> newPosition: aPosition [

	^ self basicNew 
		initializePosition: aPosition;
		yourself
]

{ #category : #accessing }
PgxStar class >> sizes [

	^ #(1 3 6 10)
]

{ #category : #'as yet unclassified' }
PgxStar class >> starTypes [

	 ^ StarTypes ifNil: [ StarTypes := PgxStarType all ]
]

{ #category : #comparing }
PgxStar >> = other [

	^ self species = other species 
		and: [ self position = other position ]
]

{ #category : #accessing }
PgxStar >> color [

	^ color
]

{ #category : #accessing }
PgxStar >> diameter [

	^ diameter
]

{ #category : #generating }
PgxStar >> generate [
	| random seed presentPlanets |
	
	seed := ((position x bitAnd: 16rFFFF) << 16) | (position y bitAnd: 16rFFFF).
	random := self randomClass seed: seed.
	
	hasStar := (random nextInteger: 20) = 1.
	"nothing else to do"
	hasStar ifFalse: [ ^ self ].
	 
	type := random nextCollection: self class starTypes.
	spectralType := random nextCollection: type typicalSpectralTypes.
	color := random nextCollection: type typicalColors.
	diameter := random nextCollection: type typicalDiameter.
	
	presentPlanets := random nextCollection: type planetPresence.
	presentPlanets > 0 ifTrue: [  
		planets := Array new: presentPlanets.
		1 to: presentPlanets do: [ :index |
			planets at: index put: (PgxPlanet new 
				generateWith: random;
				yourself) ] ]
]

{ #category : #testing }
PgxStar >> hasStar [

	^ hasStar
]

{ #category : #comparing }
PgxStar >> hash [

	^ self species hash 
		bitXor: self position hash
]

{ #category : #initialization }
PgxStar >> initialize [

	super initialize.
	self generate
]

{ #category : #initialization }
PgxStar >> initializePosition: aPosition [

	position := aPosition.
	self initialize
]

{ #category : #accessing }
PgxStar >> numPlanets [

	^ self planets size
]

{ #category : #accessing }
PgxStar >> numPlanetsAsString [
	| numPlanets |

	numPlanets := self numPlanets.
	numPlanets isZero ifTrue: [ ^ 'None' ].
	
	^ numPlanets asString
]

{ #category : #accessing }
PgxStar >> planets [ 

	^ planets ifNil: [ #() ]
]

{ #category : #accessing }
PgxStar >> position [

	^ position
]

{ #category : #accessing }
PgxStar >> radius [

	^ self diameter / 2.0
]

{ #category : #generating }
PgxStar >> randomClass [
	
	^ LehmerRandom
]

{ #category : #accessing }
PgxStar >> spectralType [

	^ spectralType
]

{ #category : #accessing }
PgxStar >> type [

	^ type
]

{ #category : #accessing }
PgxStar >> typeName [

	^ self type name
]
