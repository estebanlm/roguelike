Class {
	#name : #PgxGalaxyZone,
	#superclass : #Object,
	#instVars : [
		'zone'
	],
	#category : #'ProceduralGalaxy-Model'
}

{ #category : #'instance creation' }
PgxGalaxyZone class >> newZone: aRectangle [

	^ self basicNew
		initializeZone: aRectangle;
		yourself
]

{ #category : #generating }
PgxGalaxyZone >> generateWithSystemDo: aBlock [
	| sectors x y system |

	sectors := (zone extent / self sectorSize) asIntegerPoint.
	0 to: (sectors x - 1) do: [ :sectorX |	
		0 to: (sectors y - 1) do: [ :sectorY |
			x := sectorX + zone left.
			y := sectorY + zone top.
			system := PgxStar newPosition: x@y.
			system hasStar 
				ifTrue: [ aBlock value: system ] ] ]
]

{ #category : #initialization }
PgxGalaxyZone >> initializeZone: aRectangle [

	self initialize.
	zone := aRectangle

]

{ #category : #private }
PgxGalaxyZone >> positionToSectorX: pX [
	| sizeX sectorX |

	sectorX := 0.
	sizeX := self sectorSize x.
	0 to: zone width asInteger by: sizeX do: [ :x |
		(pX between: x and: (x + sizeX))
			ifTrue: [ ^ (sectorX + zone left) ].
		sectorX := sectorX + 1 ].
		
	self error: 'Position X is not in this zone!'

]

{ #category : #private }
PgxGalaxyZone >> positionToSectorY: pY [
	| sizeY sectorY |

	sectorY := 0.
	sizeY := self sectorSize y.
	0 to: zone height asInteger by: sizeY do: [ :y |
		(pY between: y and: (y + sizeY))
			ifTrue: [ ^ sectorY + zone top ].
		sectorY := sectorY + 1 ].
		
	self error: 'Position Y is not in this zone!'
]

{ #category : #accessing }
PgxGalaxyZone >> sectorSize [

	^ 16@16
]

{ #category : #accessing }
PgxGalaxyZone >> systemAt: aPoint [

	^ PgxStar newPosition: (self toZonePosition: aPoint)
]

{ #category : #accessing }
PgxGalaxyZone >> systemPresencePercent [

	^ 10
]

{ #category : #converting }
PgxGalaxyZone >> toWindowRectangle: aSystem [

	^
		((aSystem position-zone topLeft)*self sectorSize)+(self sectorSize/2.0)-(aSystem radius@aSystem radius)
		extent: (aSystem diameter@aSystem diameter)
]

{ #category : #converting }
PgxGalaxyZone >> toZonePosition: aPoint [

	^ (self positionToSectorX: aPoint x)@(self positionToSectorY: aPoint y)
]
