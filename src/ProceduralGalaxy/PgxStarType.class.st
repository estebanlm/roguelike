Class {
	#name : #PgxStarType,
	#superclass : #Object,
	#instVars : [
		'name',
		'typicalSpectralTypes',
		'typicalColors',
		'typicalDiameter',
		'planetPresence'
	],
	#category : #'ProceduralGalaxy-Model'
}

{ #category : #accessing }
PgxStarType class >> all [ 

	^ { 
	self blue.
	self blueGiant.
	self blueSuperGiant.
	self brownDwarf.
	self orangeDwarf.
	self redDwarf.
	self redGiant.
	self redSuperGiant.
	self whiteDwarf.
	self yellowDwarf
	 }
]

{ #category : #accessing }
PgxStarType class >> blue [
	"colors from http://www.vendian.org/mncharity/dir3/starcolor/details.html"

	^ self new 
		name: 'Blue';
		typicalSpectralTypes: #(O B);
		typicalColors: #('9bb2ff' 'b2c5ff' 'ccd8ff' 'dfe5ff');
		typicalDiameter: (6 to: 16);
		planetPresence: (0 to: 15);
		yourself
]

{ #category : #accessing }
PgxStarType class >> blueGiant [
	"colors from http://www.vendian.org/mncharity/dir3/starcolor/details.html"

	^ self new 
		name: 'Blue Giant';
		typicalSpectralTypes: #(O B A);		
		typicalColors: #('9bb2ff' 'b2c5ff' 'ccd8ff' 'dfe5ff');
		typicalDiameter: (10 to: 15);
		planetPresence: (0 to: 15);
		yourself
]

{ #category : #accessing }
PgxStarType class >> blueSuperGiant [
	"colors from http://www.vendian.org/mncharity/dir3/starcolor/details.html"

	^ self new 
		name: 'Blue Super Giant';
		typicalSpectralTypes: #(O B);		
		typicalColors: #('9bb2ff' 'b2c5ff' 'ccd8ff' 'dfe5ff');
		typicalDiameter: (15 to: 20);
		planetPresence: (0 to: 15);
		yourself
]

{ #category : #accessing }
PgxStarType class >> brownDwarf [
	"colors from http://www.vendian.org/mncharity/dir3/starcolor/details.html"

	^ self new 
		name: 'Brown Dwarf';
		typicalSpectralTypes: #(M L T Y);		
		typicalColors: #('A52A2A' 'A66829');
		typicalDiameter: (2 to: 5);
		planetPresence: (0 to: 10);
		yourself
]

{ #category : #accessing }
PgxStarType class >> orangeDwarf [
	"colors from http://www.vendian.org/mncharity/dir3/starcolor/details.html"

	^ self new 
		name: 'Orange Dwarf';
		typicalSpectralTypes: #(K);
		typicalColors: #('ffd29c' 'ffcc8f' 'ffc178' 'ff9523');
		typicalDiameter: (2 to: 5);
		planetPresence: (0 to: 10);
		yourself
]

{ #category : #accessing }
PgxStarType class >> redDwarf [
	"colors from http://www.vendian.org/mncharity/dir3/starcolor/details.html"

	^ self new 
		name: 'Red Dwarf';
		typicalSpectralTypes: #(K M);
		typicalColors: #('ffd29c' 'ffd096' 'ffcc8f' 'ffc885' 'ff7b00' 'ff5200');
		typicalDiameter: (2 to: 5);
		planetPresence: (0 to: 10);
		yourself
]

{ #category : #accessing }
PgxStarType class >> redGiant [
	"colors from http://www.vendian.org/mncharity/dir3/starcolor/details.html"

	^ self new 
		name: 'Red Giant';
		typicalSpectralTypes: #(M K);		
		typicalColors: #('ffd29c' 'ffd096' 'ffcc8f' 'ffc885' 'ff7b00' 'ff5200');
		typicalDiameter: (10 to: 15);
		planetPresence: (0 to: 10);
		yourself
]

{ #category : #accessing }
PgxStarType class >> redSuperGiant [
	"colors from http://www.vendian.org/mncharity/dir3/starcolor/details.html"

	^ self new 
		name: 'Red Super Giant';
		typicalSpectralTypes: #(M K);
		typicalColors: #('ffd29c' 'ffd096' 'ffcc8f' 'ffc885' 'ff7b00' 'ff5200');
		typicalDiameter: (15 to: 20);
		planetPresence: (0 to: 10);
		yourself
]

{ #category : #accessing }
PgxStarType class >> whiteDwarf [
	"colors from http://www.vendian.org/mncharity/dir3/starcolor/details.html"

	^ self new 
		name: 'White Dwarf';
		typicalSpectralTypes: #(D);		
		typicalColors: #('ffffff' 'eeefff' 'f3f2ff');
		typicalDiameter: (2 to: 4);
		planetPresence: (0 to: 10);
		yourself
]

{ #category : #accessing }
PgxStarType class >> yellowDwarf [
	"colors from http://www.vendian.org/mncharity/dir3/starcolor/details.html"

	^ self new 
		name: 'Yellow Dwarf';
		typicalSpectralTypes: #(G);
		typicalColors: #('fff5ef' 'fff1e5' 'ffeddb' 'ffe9d2');
		typicalDiameter: (2 to: 4);
		planetPresence: (0 to: 10);
		yourself
]

{ #category : #accessing }
PgxStarType >> name [

	^ name
]

{ #category : #accessing }
PgxStarType >> name: anObject [

	name := anObject
]

{ #category : #accessing }
PgxStarType >> planetPresence [

	^ planetPresence
]

{ #category : #accessing }
PgxStarType >> planetPresence: aRange [

	planetPresence := aRange
]

{ #category : #accessing }
PgxStarType >> typicalColors [

	^ typicalColors
]

{ #category : #accessing }
PgxStarType >> typicalColors: aCollectionOfHex [

	typicalColors := aCollectionOfHex collect: [ :each | Color fromHexString: each ]
]

{ #category : #accessing }
PgxStarType >> typicalDiameter [

	^ typicalDiameter
]

{ #category : #accessing }
PgxStarType >> typicalDiameter: anObject [

	typicalDiameter := anObject
]

{ #category : #accessing }
PgxStarType >> typicalSpectralTypes [

	^ typicalSpectralTypes
]

{ #category : #accessing }
PgxStarType >> typicalSpectralTypes: anObject [

	typicalSpectralTypes := anObject
]
