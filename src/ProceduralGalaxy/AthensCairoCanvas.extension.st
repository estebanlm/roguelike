Extension { #name : #AthensCairoCanvas }

{ #category : #'*ProceduralGalaxy' }
AthensCairoCanvas >> arcCenterX: xc centerY: yc radius: radius startAngle: angle1 endAngle: angle2 [ 

	^ self ffiCall: #(void cairo_arc (self,
				double xc,
				double yc,
				double radius,
				double angle1,
				double angle2) )
]

{ #category : #'*ProceduralGalaxy' }
AthensCairoCanvas >> arcNegativeCenterX: xc centerY: yc radius: radius startAngle: angle1 endAngle: angle2 [ 

	^ self ffiCall: #(void cairo_arc_negative (self,
				double xc,
				double yc,
				double radius,
				double angle1,
				double angle2) )
]
