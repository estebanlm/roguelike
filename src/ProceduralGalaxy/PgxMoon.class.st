Class {
	#name : #PgxMoon,
	#superclass : #Object,
	#instVars : [
		'diameter'
	],
	#category : #'ProceduralGalaxy-Model'
}

{ #category : #accessing }
PgxMoon >> diameter [

	^ diameter
]

{ #category : #generating }
PgxMoon >> generateWith: aRandom [

	diameter := 0.6
]

{ #category : #accessing }
PgxMoon >> radius [
	
	^ self diameter / 2.0
]
