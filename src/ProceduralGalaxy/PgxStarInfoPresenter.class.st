Class {
	#name : #PgxStarInfoPresenter,
	#superclass : #SpPresenter,
	#traits : 'SpTModel',
	#classTraits : 'SpTModel classTrait',
	#instVars : [
		'athensPresenter'
	],
	#category : #'ProceduralGalaxy-View'
}

{ #category : #private }
PgxStarInfoPresenter >> calculateImageHeight [

	^ (self model diameter) 
		+ (self model planets 
			ifNotEmpty: [ :planets | 
				planets max: [ :each | 
					each moons 
						ifNotEmpty: [ :moons | moons sum: [ :eachMoon | (eachMoon diameter + 1) ] ]
						ifEmpty: [ 0 ] ] ]
			ifEmpty: [ 0 ]) 
		+ 2
]

{ #category : #private }
PgxStarInfoPresenter >> calculateImageWidth [

	^ (self model planets 
		inject: self model diameter + 1
		into: [ :all :each | all + each diameter + 1 ])
		+ 2
]

{ #category : #'private - drawing' }
PgxStarInfoPresenter >> drawBackgroundOn: aCanvas bounds: aRectangle [
	
	aCanvas surface loadSolidColor: Color black.
	aCanvas 
		primRectangleX: 0.0 
		y: 0.0
		width: aRectangle width asFloat 
		height: aRectangle height asFloat.
	aCanvas fill
]

{ #category : #'private - drawing' }
PgxStarInfoPresenter >> drawMoon: aMoon bounds: aRectangle dx: dx dy: dy on: aCanvas [
	| transform |
	
	transform := AthensCairoTransform for: aCanvas.
	transform restoreAfter: [ 
		transform translateX: dx asFloat Y: (dy + aMoon radius + 1).
		aCanvas surface loadSolidColor: self model color. 
		aCanvas 
			arcCenterX: 0.0
			centerY: 0.0
			radius: aMoon radius asFloat 
			startAngle: 0.0 
			endAngle: Float twoPi.
		aCanvas fill ].

	^ aMoon diameter + 1
]

{ #category : #'private - drawing' }
PgxStarInfoPresenter >> drawOn: aCanvas bounds: aRectangle [

	"self drawBackgroundOn: aCanvas bounds: aRectangle."
	self drawSystemOn: aCanvas bounds: aRectangle
]

{ #category : #'private - drawing' }
PgxStarInfoPresenter >> drawPlanet: aPlanet bounds: aRectangle dx: dx on: aCanvas [
	| transform dy |
	
	transform := AthensCairoTransform for: aCanvas.
	transform restoreAfter: [ 
		transform translateX: (dx + aPlanet radius + 1) asFloat Y: 0.
		aCanvas surface loadSolidColor: aPlanet color. 
		aCanvas 
			arcCenterX: 0.0
			centerY: 0.0
			radius: aPlanet radius asFloat 
			startAngle: 0.0 
			endAngle: Float twoPi.
		aCanvas fill.

		aPlanet numMoons > 0 ifTrue: [
			dy := (aPlanet radius + 1) asFloat.
			aPlanet moons do: [ :eachMoon | 
				dy := dy + (self 
					drawMoon: eachMoon
					bounds: aRectangle
					dx: 0
					dy: dy
					on: aCanvas) ] ] ].

	^ aPlanet diameter + 1
]

{ #category : #'private - drawing' }
PgxStarInfoPresenter >> drawStarOn: aCanvas bounds: aRectangle [
	| transform |
	
	transform := AthensCairoTransform for: aCanvas.
	transform restoreAfter: [ 
		transform translateX: self model radius asFloat Y: 0.
		aCanvas surface loadSolidColor: self model color. 
		aCanvas 
			arcCenterX: 0.0
			centerY: 0.0
			radius: self model radius asFloat 
			startAngle: 0.0 
			endAngle: Float twoPi.
		aCanvas fill ].

	^ self model diameter + 1
]

{ #category : #'private - drawing' }
PgxStarInfoPresenter >> drawSystemOn: aCanvas bounds: aRectangle [
	| dx transform |

	transform := AthensCairoTransform for: aCanvas.
	transform scaleX: 4.0 Y: 4.0.
	transform translateX: 2 Y: (self model radius) + 2.
	
	dx := self drawStarOn: aCanvas bounds: aRectangle.
	self model planets do: [ :each |
		dx := dx + (self 
			drawPlanet: each 
			bounds: aRectangle 
			dx: dx
			on: aCanvas) ]
]

{ #category : #initialization }
PgxStarInfoPresenter >> initializePresenters [

	self layout: (SpBoxLayout newVertical
		borderWidth: 5;
		spacing: 5;
		vAlignCenter;
		add: (athensPresenter := self newAthens);
		add: (SpGridLayout new 
			beColumnNotHomogeneous;
			column: 2 withConstraints: [ :c | c beExpand ];
			build: [ :aBuilder | aBuilder
				add: (self newTitleLabel: 'Position'); add: self model position asString; nextRow;
				add: (self newTitleLabel: 'Type'); add: self model typeName; nextRow;
				add: (self newTitleLabel: 'Spectral type'); add: self model spectralType asString; nextRow;
				add: (self newTitleLabel: 'Radius'); add: self model radius; nextRow;
				add: (self newTitleLabel: 'Planets'); add: self model numPlanetsAsString ];
			yourself);
		yourself).
			
	athensPresenter 
		surfaceExtent: (self calculateImageWidth * 4)@(self calculateImageHeight * 4);
		drawBlock: [ :aCanvas :bounds | self drawOn: aCanvas bounds: bounds ]
]

{ #category : #initialization }
PgxStarInfoPresenter >> newTitleLabel: aString [

	^ self newLabel 
		label: aString;
		addStyle: 'header';
		yourself
]
