Class {
	#name : #PgxGtkConfiguration,
	#superclass : #SpGtkConfiguration,
	#category : #'ProceduralGalaxy-Application'
}

{ #category : #configuring }
PgxGtkConfiguration >> configureGeneral: anApplication [

	self addCSSProviderFromString: '
label.header { font-weight: bold; }
'
]
