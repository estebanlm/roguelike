Class {
	#name : #PgxGalaxyViewPresenter,
	#superclass : #SpPresenter,
	#instVars : [
		'position',
		'offset',
		'zoneAthensPresenter',
		'selectedSystem',
		'lastZone',
		'popover'
	],
	#category : #'ProceduralGalaxy-View'
}

{ #category : #private }
PgxGalaxyViewPresenter >> computePosibleSelection: aPoint [
	| system |

	system := lastZone systemAt: aPoint asIntegerPoint.
	system hasStar 
		ifTrue: [ 
			self setSelection: system.
			self showPopoverForSelectedSystem ]
		ifFalse: [ 
			self resetSelection.
			self dismissPopover ]
]

{ #category : #'private - showing' }
PgxGalaxyViewPresenter >> dismissPopover [

	popover ifNil: [ ^ self ].
	popover dismiss.
	popover := nil
]

{ #category : #'private - drawing' }
PgxGalaxyViewPresenter >> drawBackgroundOn: aCanvas bounds: aRectangle [
	
	aCanvas surface loadSolidColor: Color black.
	aCanvas 
		primRectangleX: 0.0 
		y: 0.0
		width: aRectangle width asFloat 
		height: aRectangle height asFloat.
	aCanvas fill
]

{ #category : #'private - drawing' }
PgxGalaxyViewPresenter >> drawOn: aCanvas bounds: aRectangle [
	| time |
	
	self drawBackgroundOn: aCanvas bounds: aRectangle.
	time := [ self drawZoneOn: aCanvas bounds: aRectangle ] timeToRun.
	self flag: #HACK. "Because I do not know how to reset the 'translate', I need 
	to move witht this magic numbers"
	aCanvas moveToX: 10.0 asFloat Y: 20.0.
	self drawTime: time on: aCanvas
]

{ #category : #'private - drawing' }
PgxGalaxyViewPresenter >> drawSystem: aSystem on: aCanvas [

	aCanvas surface loadSolidColor: aSystem color. 
	aCanvas 
		arcCenterX: 0.0
		centerY: 0.0
		radius: aSystem radius asFloat 
		startAngle: 0.0 
		endAngle: Float twoPi.
	aCanvas fill
]

{ #category : #'private - drawing' }
PgxGalaxyViewPresenter >> drawSystemSelection: aSystem on: aCanvas [

	"aCanvas primSetLineWidth: 1.0."
	self selectionColor loadOnCairoCanvas: aCanvas. 
	aCanvas 
		arcCenterX: 0.0
		centerY: 0.0
		radius: aSystem radius asFloat + 5
		startAngle: 0.0 
		endAngle: Float twoPi.
	aCanvas stroke
]

{ #category : #'private - drawing' }
PgxGalaxyViewPresenter >> drawSystemsOn: aCanvas extent: aSize [
	| transform |

	transform := AthensCairoTransform for: aCanvas.
	(lastZone := self galaxyZoneOn: (1@1 extent: aSize)) 
		generateWithSystemDo: [ :each | 
			transform restoreAfter: [  
				transform 
					translateX: (((each position x)-(offset x)-1)*16)+8 asFloat 
					Y: (((each position y)-(offset y)-1)*16)+8 asFloat.  
				self drawSystem: each on: aCanvas.
				(self isSelected: each) 
					ifTrue: [ self drawSystemSelection: each on: aCanvas ] ] ]
]

{ #category : #'private - drawing' }
PgxGalaxyViewPresenter >> drawTime: time on: aCanvas [
	| transform |

	transform := AthensCairoTransform for: aCanvas.
	transform restoreAfter: [ 
		transform translateX: 10.0 Y: 20.0.
		aCanvas surface loadSolidColor: Color white.
		aCanvas showText: ('Time: {1}' format: { time }) ]
]

{ #category : #'private - drawing' }
PgxGalaxyViewPresenter >> drawZoneOn: aCanvas bounds: aRectangle [

	self drawSystemsOn: aCanvas extent: aRectangle extent.
	self drawZoneRegionOn: aCanvas bounds: aRectangle
]

{ #category : #'private - drawing' }
PgxGalaxyViewPresenter >> drawZoneRegionOn: aCanvas bounds: aRectangle [
	| transform text textExtents |

	transform := AthensCairoTransform for: aCanvas.
	transform restoreAfter: [  
		text := (aRectangle translateBy: self offset) asString.
		textExtents := CairoFontExtents new.
		aCanvas surface loadSolidColor: Color white.
		aCanvas text: text extents: textExtents.
		transform 
			translateX: (aRectangle right - textExtents width - 35) asFloat 
			Y: 20.0.  
		aCanvas showText: text.
		transform loadIdentity ]
]

{ #category : #private }
PgxGalaxyViewPresenter >> galaxyZoneOn: aRectangle [

	^ PgxGalaxyZone newZone: (aRectangle translateBy: self offset)

]

{ #category : #'private - accessing' }
PgxGalaxyViewPresenter >> gap [

	^ 2
]

{ #category : #initialization }
PgxGalaxyViewPresenter >> initialize [

	super initialize.
	offset := 0@0
]

{ #category : #initialization }
PgxGalaxyViewPresenter >> initializePresenters [

	self layout: (SpOverlayLayout new 
		child: (zoneAthensPresenter := self newAthens);
		yourself).
		
	zoneAthensPresenter drawBlock: [ :aCanvas :aRectangle | 
		self drawOn: aCanvas bounds: aRectangle ].

	zoneAthensPresenter 
		bindKeyCombination: Character arrowLeft asKeyCombination 
			toAction: [ self setOffset: (self gap negated)@0 ];
		bindKeyCombination: Character arrowRight asKeyCombination 
			toAction: [ self setOffset: (self gap)@0 ];
		bindKeyCombination: Character arrowUp asKeyCombination 
			toAction: [ self setOffset: 0@(self gap negated) ];
		bindKeyCombination: Character arrowDown asKeyCombination
			toAction: [ self setOffset: 0@(self gap) ].
			
	zoneAthensPresenter eventHandler
		whenDoubleClickDo: [ :anEvent | self showSelectedSystem ];
		whenMouseMoveDo: [ :anEvent | self computePosibleSelection: anEvent position ]
]

{ #category : #initialization }
PgxGalaxyViewPresenter >> initializeWindow: aWindowPresenter [

	aWindowPresenter 
		title: 'Galaxy';
		initialExtent: 800@600
]

{ #category : #'private - testing' }
PgxGalaxyViewPresenter >> isSelected: aSystem [

	^ selectedSystem = aSystem
]

{ #category : #'private - accessing' }
PgxGalaxyViewPresenter >> offset [

	^ offset
]

{ #category : #private }
PgxGalaxyViewPresenter >> redraw [

	zoneAthensPresenter redraw
]

{ #category : #private }
PgxGalaxyViewPresenter >> resetSelection [

	self setSelection: nil
]

{ #category : #'private - accessing' }
PgxGalaxyViewPresenter >> selectionColor [

	^ Color white
]

{ #category : #private }
PgxGalaxyViewPresenter >> setOffset: aPoint [ 

	offset := offset + aPoint.
	zoneAthensPresenter redraw
]

{ #category : #private }
PgxGalaxyViewPresenter >> setSelection: aSystem [
		
	selectedSystem = aSystem ifTrue: [ ^ self ].
	selectedSystem := aSystem.
	self redraw
]

{ #category : #'private - showing' }
PgxGalaxyViewPresenter >> showPopoverForSelectedSystem [
		
	popover ifNotNil: [ 
		popover presenter model = selectedSystem
			ifFalse: [ self dismissPopover ]
			ifTrue: [ ^ self ] ].

	(popover := self newPopover) 
		presenter: (self instantiate: PgxStarInfoPresenter on: selectedSystem);
		popupPointingTo: (lastZone toWindowRectangle: selectedSystem)
]

{ #category : #private }
PgxGalaxyViewPresenter >> showSelectedSystem [

	self flag: #TODO.
]
