Class {
	#name : #PgxPlanetType,
	#superclass : #Object,
	#instVars : [
		'name',
		'typicalDiameter',
		'moonsPresence',
		'typicalColors'
	],
	#category : #'ProceduralGalaxy-Model'
}

{ #category : #accessing }
PgxPlanetType class >> all [

	^ { 
	self terrestrialCarbon.
	self terrestrialDesert.
	self terrestrialIce.
	self terrestrialIron.
	self terrestrialLava.
	self terrestrialOcean.
	self terrestrialSilicate. "earth"
	self gasDwarf.
	self gasGiant.
	self iceGiant.
	self puffy
	 }
]

{ #category : #accessing }
PgxPlanetType class >> gasDwarf [

	^ self new 
		name: 'Gas Dwarf';
		typicalDiameter: (2 to: 3);
		typicalColors: #('b2a59d' 'd8cbb0' 'd0a47a' 'e7dee3' 'dbae73');		
		moonsPresence: (-3 to: 8);
		yourself
]

{ #category : #accessing }
PgxPlanetType class >> gasGiant [

	^ self new 
		name: 'Gas Giant';
		typicalDiameter: (3 to: 6);
		typicalColors: #('855e00' 'ceeae7' 'eae3a2' 'ce943c' '560404');		
		moonsPresence: (-2 to: 12);
		yourself
]

{ #category : #accessing }
PgxPlanetType class >> iceGiant [

	^ self new 
		name: 'Ice Giant';
		typicalDiameter: (3 to: 6); 
		typicalColors: #('ffffff' 'b9e8ea' '86d6d8' '3fd0d4' '20c3d0');
		moonsPresence: (-2 to: 12);
		yourself
]

{ #category : #accessing }
PgxPlanetType class >> puffy [

	^ self new 
		name: 'Puffy';
		typicalDiameter: (2 to: 4);
		typicalColors: #('ceb8b8' 'ead6b8' 'e3e0c0' 'bfbdaf' 'cecece');
		moonsPresence: (-2 to: 6);
		yourself
]

{ #category : #accessing }
PgxPlanetType class >> terrestrialCarbon [

	^ self new 
		name: 'Carbon';
		typicalDiameter: (1 to: 2);
		typicalColors: #('a484d3' '6f4792' '2f1b47' 'bbbbbb' 'aaaaaa');
		moonsPresence: (-2 to: 4);
		yourself
]

{ #category : #accessing }
PgxPlanetType class >> terrestrialDesert [

	^ self new 
		name: 'Desert';
		typicalDiameter: (1 to: 2); 
		typicalColors: #('fbec85' 'e3d465' 'd4c449' 'baaa2e' 'a7971a');
		moonsPresence: (-2 to: 4);		
		yourself
]

{ #category : #accessing }
PgxPlanetType class >> terrestrialIce [

	^ self new 
		name: 'Ice';
		typicalDiameter: (1 to: 2); 
		typicalColors: #('ffffff' 'b9e8ea' '86d6d8' '3fd0d4' '20c3d0');
		moonsPresence: (-2 to: 4);		
		yourself
]

{ #category : #accessing }
PgxPlanetType class >> terrestrialIron [

	^ self new 
		name: 'Iron';
		typicalDiameter: (1 to: 2);
		typicalColors: #('95A3A6' 'F2F2F2' 'F2F2F2' 'A59C94' '66615C' 'F2E5DA' 'B3A9A1' '8C857E');
		moonsPresence: (-2 to: 4);		
		yourself
]

{ #category : #accessing }
PgxPlanetType class >> terrestrialLava [

	^ self new 
		name: 'Lava';
		typicalDiameter: (1 to: 2); 
		typicalColors: #('ff2500' 'ff6600' 'f2f217' 'ea5c0f' 'e56520');
		moonsPresence: (-2 to: 4);
		yourself
]

{ #category : #accessing }
PgxPlanetType class >> terrestrialOcean [

	^ self new 
		name: 'Ocean';
		typicalDiameter: (1 to: 2); 
		typicalColors: #('064273' '76b6c4' '7fcdff' '1da2d8' 'def3f6');
		moonsPresence: (-2 to: 4);
		yourself
]

{ #category : #accessing }
PgxPlanetType class >> terrestrialSilicate [

	^ self new 
		name: 'Silicate';
		typicalDiameter: (1 to: 2);
		typicalColors: #('d8c596' '9fc164' 'e9eff9' '6b93d6' '4f4cb0'); 
		moonsPresence: (-2 to: 4);
		yourself
]

{ #category : #accessing }
PgxPlanetType >> moonsPresence [

	^ moonsPresence
]

{ #category : #accessing }
PgxPlanetType >> moonsPresence: aRange [
	"values <= 0 would mean no moon. This is a nice trick to avoid calculate presence first 
	 and amount of them later. 
	 Eg.e -5 to: 5 would mean 50% change of presence and up to 4 moons (since min:max: interval 
	 is not inclusive."
	moonsPresence := aRange
]

{ #category : #accessing }
PgxPlanetType >> name [

	^ name
]

{ #category : #accessing }
PgxPlanetType >> name: anObject [

	name := anObject
]

{ #category : #accessing }
PgxPlanetType >> typicalColors [

	^ typicalColors
]

{ #category : #accessing }
PgxPlanetType >> typicalColors: aCollectionOfColors [

	typicalColors := aCollectionOfColors collect: [ :each | Color fromHexString: each ]
]

{ #category : #accessing }
PgxPlanetType >> typicalDiameter [

	^ typicalDiameter
]

{ #category : #accessing }
PgxPlanetType >> typicalDiameter: anObject [

	typicalDiameter := anObject
]
