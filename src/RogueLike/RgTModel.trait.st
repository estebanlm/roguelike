Trait {
	#name : #RgTModel,
	#instVars : [
		'model'
	],
	#category : #'RogueLike-View'
}

{ #category : #transmission }
RgTModel >> defaultInputPort [

	^ self inputModelPort
]

{ #category : #transmission }
RgTModel >> inputModelPort [

	^ SpModelPort newPresenter: self
]

{ #category : #'accessing model' }
RgTModel >> model [

	^ model
]

{ #category : #'accessing model' }
RgTModel >> model: aModel [

	model := aModel.
	self updatePresenter
]

{ #category : #private }
RgTModel >> setModel: aModel [

	self model: aModel
]

{ #category : #'accessing model' }
RgTModel >> setModelBeforeInitialization: aModel [

	model := aModel
]
