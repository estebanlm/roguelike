Class {
	#name : #RgLanguage,
	#superclass : #Object,
	#instVars : [
		'dictionary'
	],
	#category : #'RogueLike-Language'
}

{ #category : #'reflective operations' }
RgLanguage >> doesNotUnderstand: aMessage [

	aMessage selector isUnary ifFalse: [ ^ super doesNotUnderstand: aMessage ].
	
	^ dictionary at: aMessage selector
]

{ #category : #initialization }
RgLanguage >> initialize [

	super initialize.
	dictionary := self newDictionary
]

{ #category : #'instance creation' }
RgLanguage >> newDictionary [

	^ self subclassResponsibility
]
