Class {
	#name : #RgEntity,
	#superclass : #Object,
	#instVars : [
		'tile'
	],
	#category : #'RogueLike-Model'
}

{ #category : #printing }
RgEntity >> color [

	^ nil
]

{ #category : #testing }
RgEntity >> isActor [

	^ false
]

{ #category : #testing }
RgEntity >> isBlocking [

	^ self subclassResponsibility
]

{ #category : #testing }
RgEntity >> isHostile [

	^ false
]

{ #category : #testing }
RgEntity >> isItem [

	^ false
]

{ #category : #testing }
RgEntity >> isPlayer [

	^ false
]

{ #category : #testing }
RgEntity >> isWalkable [
	
	^ self subclassResponsibility
]

{ #category : #accessing }
RgEntity >> map [

	^ self tile map
]

{ #category : #accessing }
RgEntity >> name [

	^ self subclassResponsibility
]

{ #category : #'as yet unclassified' }
RgEntity >> playTurn [
	"Do nothing"
	^ RgNoAction for: self
]

{ #category : #accessing }
RgEntity >> position [

	^ self tile position
]

{ #category : #rendering }
RgEntity >> renderOn: aConsole [
	
	^ aConsole renderEntity: self
]

{ #category : #printing }
RgEntity >> symbol [

	^ self subclassResponsibility
]

{ #category : #accessing }
RgEntity >> tile [

	^ tile
]

{ #category : #accessing }
RgEntity >> tile: aTile [

	tile := aTile
]

{ #category : #printing }
RgEntity >> visibleDescription [

	^ ''
]
