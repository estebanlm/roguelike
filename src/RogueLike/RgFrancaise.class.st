Class {
	#name : #RgFrancaise,
	#superclass : #RgLanguage,
	#category : #'RogueLike-Language'
}

{ #category : #'instance creation' }
RgFrancaise >> newDictionary [

	^ Dictionary newFromPairs: { 
	#titleInventory. 'Inventaire'.
	#titleInventoryForUse. 'Utiliser'.
	#messageInventoryEmpty. 'Ton inventaire est vide.'
	 }
]
