Class {
	#name : #RgHostile,
	#superclass : #RgActor,
	#category : #'RogueLike-Model-Actors'
}

{ #category : #'as yet unclassified' }
RgHostile >> aliveSymbol [

	^ self subclassResponsibility
]

{ #category : #printing }
RgHostile >> color [

	^ Color yellow
]

{ #category : #'as yet unclassified' }
RgHostile >> deadSymbol [

	^ $x
]

{ #category : #testing }
RgHostile >> isBlocking [

	^ self isAlive
]

{ #category : #testing }
RgHostile >> isHostile [

	^ true
]

{ #category : #testing }
RgHostile >> isHostileTo: anActor [

	^ anActor isPlayer
]

{ #category : #'as yet unclassified' }
RgHostile >> playTurn [

	^ self isAlive 
		ifTrue: [ super playTurn ]
		ifFalse: [ RgNoAction for: self ]

]

{ #category : #removing }
RgHostile >> removeCorpse [
	
	"self map removeEntity: self"
]

{ #category : #'as yet unclassified' }
RgHostile >> symbol [

	^ self isAlive 
		ifTrue: [ self aliveSymbol ]
		ifFalse: [ self deadSymbol ]
]
