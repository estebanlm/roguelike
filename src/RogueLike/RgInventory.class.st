Class {
	#name : #RgInventory,
	#superclass : #Object,
	#instVars : [
		'owner',
		'capacity',
		'items'
	],
	#category : #'RogueLike-Model'
}

{ #category : #accessing }
RgInventory class >> defaultCapacity [

	^ 10
]

{ #category : #'instance creation' }
RgInventory class >> on: anOwner [

	^ self new 
		owner: anOwner;
		yourself
]

{ #category : #accessing }
RgInventory >> addItem: anItem [

	items size = self capacity 
		ifTrue: [ RgImpossible signal: 'Your inventory is full!' ].
	items add: anItem
]

{ #category : #accessing }
RgInventory >> capacity [

	^ capacity
]

{ #category : #accessing }
RgInventory >> capacity: anObject [

	capacity := anObject
]

{ #category : #actions }
RgInventory >> dropItem: anItem [

	items remove: anItem.
	self owner map
		placeEntity: anItem 
		at: self owner position
]

{ #category : #initialization }
RgInventory >> initialize [

	super initialize.
	capacity := self class defaultCapacity.
	items := OrderedCollection new
]

{ #category : #accessing }
RgInventory >> itemAt: aNumber [

	^ self items at: aNumber
]

{ #category : #accessing }
RgInventory >> items [

	^ items
]

{ #category : #accessing }
RgInventory >> owner [

	^ owner
]

{ #category : #accessing }
RgInventory >> owner: anObject [

	owner := anObject
]

{ #category : #actions }
RgInventory >> pickItem: anItem [

	self addItem: anItem.
	self owner map
		removeEntity: anItem 
		from: self owner position
]

{ #category : #accessing }
RgInventory >> removeItem: anItem [ 

	items remove: anItem
]
