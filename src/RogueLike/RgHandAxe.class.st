Class {
	#name : #RgHandAxe,
	#superclass : #RgEquipment,
	#category : #'RogueLike-Model-Items'
}

{ #category : #actions }
RgHandAxe >> doActivate: anAction [
	| target targetTile |

	target := anAction console pickTarget.
	targetTile := target tile.
	RgMeleeAction newActor: anAction actor tile: targetTile.
	anAction actor inventory 
		removeItem: self.
	targetTile map 
		placeEntity: self 
		at: targetTile position
]

{ #category : #accessing }
RgHandAxe >> name [

	^ 'Hache de main'
]

{ #category : #printing }
RgHandAxe >> symbol [

	^ '7'
]
