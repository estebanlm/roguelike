Extension { #name : #KeyboardKey }

{ #category : #'*RogueLike' }
KeyboardKey class >> period [

	^ self named: 'PERIOD'
]
