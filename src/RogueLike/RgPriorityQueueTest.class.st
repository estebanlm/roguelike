Class {
	#name : #RgPriorityQueueTest,
	#superclass : #TestCase,
	#instVars : [
		'queue'
	],
	#category : #'RogueLike-Utils'
}

{ #category : #running }
RgPriorityQueueTest >> setUp [
	
	super setUp.
	queue := RgPriorityQueue new
]

{ #category : #tests }
RgPriorityQueueTest >> testAtPut [
	
	queue at: 42 put: 'three' .
	queue at: 1 put: 'one'. 
	queue at: 20 put: 'two'.
	
	self assert: queue prioritizedList equals: { 'one'. 'two'. 'three' }
]

{ #category : #tests }
RgPriorityQueueTest >> testGet [
	
	queue at: 42 put: 'three' .
	queue at: 1 put: 'one'. 
	queue at: 20 put: 'two'.
	
	self assert: queue get equals: 'one'.
	self assert: queue get equals: 'two'.
	self assert: queue get equals: 'three'.
	self 
		should: [ queue get ] 
		raise: Error
]

{ #category : #tests }
RgPriorityQueueTest >> testIsEmpty [
	
	self assert: queue isEmpty.
	queue at: 1 put: 'one'. 
	self deny: queue isEmpty.
	queue get. 
	self assert: queue isEmpty.
	
]
