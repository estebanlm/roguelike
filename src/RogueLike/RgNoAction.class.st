Class {
	#name : #RgNoAction,
	#superclass : #RgAction,
	#category : #'RogueLike-Actions'
}

{ #category : #execution }
RgNoAction >> execute [

	"No action"
]
