Class {
	#name : #RgKeyboardEventHandler,
	#superclass : #Object,
	#instVars : [
		'dispatcher',
		'defaultAction'
	],
	#category : #'RogueLike-View'
}

{ #category : #compatibility }
RgKeyboardEventHandler >> bindKeyCombination: aCombination toAction: aBlock [ 

	^ self dispatcher 
		bindKeyCombination: aCombination 
		toAction: aBlock
]

{ #category : #accessing }
RgKeyboardEventHandler >> defaultAction [

	^ defaultAction
]

{ #category : #accessing }
RgKeyboardEventHandler >> defaultAction: aBlock [

	defaultAction := aBlock
]

{ #category : #accessing }
RgKeyboardEventHandler >> dispatcher [

	^ dispatcher
]

{ #category : #events }
RgKeyboardEventHandler >> handleKeyEvent: anEvent [

	self dispatcher dispatchKeyEvent: anEvent.
	anEvent wasHandled ifTrue: [ ^ self ].
	defaultAction ifNil: [ ^ self ].
	defaultAction cull: anEvent
]

{ #category : #initialization }
RgKeyboardEventHandler >> initialize [

	super initialize.
	dispatcher := GtkKMDispatcher new
]
