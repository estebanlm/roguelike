Class {
	#name : #RgRestAction,
	#superclass : #RgAction,
	#category : #'RogueLike-Actions'
}

{ #category : #execution }
RgRestAction >> execute [

	self actor rest: 1.
	self 
		addMessage: ('{1} rests and recovers 1 hit point.' format: { self actor name })
		color: Color green
]
