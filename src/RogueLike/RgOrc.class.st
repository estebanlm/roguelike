Class {
	#name : #RgOrc,
	#superclass : #RgHostile,
	#category : #'RogueLike-Model-Actors'
}

{ #category : #printing }
RgOrc >> aliveSymbol [

	^ $o
]

{ #category : #printing }
RgOrc >> color [

	^ Color named: #brown
]

{ #category : #accessing }
RgOrc >> defense [

	^ 0
]

{ #category : #accessing }
RgOrc >> maxHitPoints [

	^ 10
]

{ #category : #accessing }
RgOrc >> name [

	^ 'Orc'
]

{ #category : #accessing }
RgOrc >> power [

	^ 3
]

{ #category : #printing }
RgOrc >> visibleDescription [

	^ 'an orc'
]
