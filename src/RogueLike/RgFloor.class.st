Class {
	#name : #RgFloor,
	#superclass : #RgTile,
	#instVars : [
		'entities'
	],
	#classVars : [
		'DarkBackgroundColor',
		'LightBackgroundColor'
	],
	#category : #'RogueLike-Model'
}

{ #category : #private }
RgFloor class >> newDarkBackgroundColor [

	^ Color r: 50 g: 50 b: 150 range: 255
]

{ #category : #private }
RgFloor class >> newLightBackgroundColor [

	^ Color named: #dodgerBlue2
]

{ #category : #adding }
RgFloor >> addEntity: anEntity [

	entities ifNil: [ entities := OrderedCollection new: 2 ].
	anEntity tile: self.
	entities add: anEntity
]

{ #category : #accessing }
RgFloor >> darkBackgroundColor [

	^ Color named: #veryDarkBlue
]

{ #category : #accessing }
RgFloor >> enemiesOf: anActor [

	^ self entities 
		select: [ :each | 
			each isActor and: [ 
				each isHostileTo: anActor ] ]
]

{ #category : #accessing }
RgFloor >> entities [

	^ entities
]

{ #category : #testing }
RgFloor >> isBlocked [

	^ self entities isEmptyOrNil not
		and: [ self entities anySatisfy: [ :each | each isBlocking ] ]
]

{ #category : #testing }
RgFloor >> isTransparent [

	^ true
]

{ #category : #testing }
RgFloor >> isWalkable [

	^ true
]

{ #category : #accessing }
RgFloor >> items [

	entities ifNil: [ ^ #() ].
	^ entities select: [ :each | each isItem ]
]

{ #category : #'accessing pathfind' }
RgFloor >> pathWeight [
	"to be used on pathfind algorithms"
	
	^ self isBlocked
		ifTrue: [ 10 ]
		ifFalse: [ 0 ]
]

{ #category : #adding }
RgFloor >> removeEntity: anEntity [

	entities remove: anEntity
]

{ #category : #rendering }
RgFloor >> renderOn: aConsole [

	super renderOn: aConsole.
	entities ifNil: [ ^ self ].
	entities do: [ :each | aConsole renderEntity: each tile: self ]
]

{ #category : #rendering }
RgFloor >> renderVisibleOn: aConsole [

	super renderVisibleOn: aConsole.
	entities ifNil: [ ^ self ].
	entities do: [ :each | aConsole renderEntity: each tile: self ]
]

{ #category : #printing }
RgFloor >> visibleDescription [
	
	self entities isEmptyOrNil 
		ifFalse: [ ^ self entities first visibleDescription ].
	
	^ ''
]
