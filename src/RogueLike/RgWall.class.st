Class {
	#name : #RgWall,
	#superclass : #RgTile,
	#category : #'RogueLike-Model'
}

{ #category : #private }
RgWall class >> newDarkBackgroundColor [

	^ Color r: 0 g: 0 b: 100 range: 255
]

{ #category : #private }
RgWall class >> newLightBackgroundColor [

	^ Color named: #dodgerBlue4
]

{ #category : #accessing }
RgWall >> darkBackgroundColor [

	^ Color named: #veryDarkBlue
]

{ #category : #accessing }
RgWall >> darkCharacter [

	^ $#
]

{ #category : #accessing }
RgWall >> darkForegroundColor [

	^ Color veryDarkGray
]

{ #category : #testing }
RgWall >> isTransparent [

	^ false
]

{ #category : #accessing }
RgWall >> lightCharacter [

	^ $#
]

{ #category : #accessing }
RgWall >> lightForegroundColor [

	^ Color darkGray
]

{ #category : #printing }
RgWall >> visibleDescription [
	
	^ 'a wall'
]
