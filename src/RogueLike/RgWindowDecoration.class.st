Class {
	#name : #RgWindowDecoration,
	#superclass : #Object,
	#instVars : [
		'window'
	],
	#category : #'RogueLike-View'
}

{ #category : #drawing }
RgWindowDecoration >> drawOn: aBuilder [

	self subclassResponsibility
]

{ #category : #accessing }
RgWindowDecoration >> window [

	^ window
]

{ #category : #accessing }
RgWindowDecoration >> window: anObject [

	window := anObject
]
