Class {
	#name : #RpPlayerStatusPresenter,
	#superclass : #SpPresenter,
	#traits : 'RgTModel',
	#classTraits : 'RgTModel classTrait',
	#instVars : [
		'healthBar'
	],
	#category : #'RogueLike-View'
}

{ #category : #initialization }
RpPlayerStatusPresenter >> initializePresenters [

	self layout: (SpBoxLayout newVertical 
		add: (healthBar := (self instantiate: MUDPercentBarPresenter)
			showBaseNumbers;
			withColorThresholds;
			yourself)
			expand: false;
		add: (self newLabel label: ('Power: {1}' format: { self player power }) ) expand: false;
		add: (self newLabel label: ('Armor: {1}' format: { self player defense })) expand: false;
		yourself).
		
	self addStyle: 'playerStatus'
]

{ #category : #accessing }
RpPlayerStatusPresenter >> player [

	^ self model player
]

{ #category : #initialization }
RpPlayerStatusPresenter >> updatePresenter [

	healthBar progress: self player hitPoints base: self player maxHitPoints
]
