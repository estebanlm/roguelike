Class {
	#name : #RgTroll,
	#superclass : #RgHostile,
	#category : #'RogueLike-Model-Actors'
}

{ #category : #printing }
RgTroll >> aliveSymbol [

	^ $T
]

{ #category : #printing }
RgTroll >> color [

	^ Color named: #forestGreen
]

{ #category : #accessing }
RgTroll >> defense [

	^ 1
]

{ #category : #accessing }
RgTroll >> maxHitPoints [

	^ 16
]

{ #category : #accessing }
RgTroll >> name [

	^ 'Troll'
]

{ #category : #accessing }
RgTroll >> power [

	^ 4
]

{ #category : #printing }
RgTroll >> visibleDescription [

	^ 'a troll'
]
