Class {
	#name : #RgConsoleBuilder,
	#superclass : #Object,
	#instVars : [
		'console',
		'lastBackgroundColor',
		'lastForegroundColor',
		'translation'
	],
	#category : #'RogueLike-View'
}

{ #category : #'instance creation' }
RgConsoleBuilder class >> new [

	self error: 'Use #on:'
]

{ #category : #'instance creation' }
RgConsoleBuilder class >> on: aConsole [

	^ self basicNew
		initializeConsole: aConsole;
		yourself
]

{ #category : #accessing }
RgConsoleBuilder >> application [
	
	^ self console application
]

{ #category : #api }
RgConsoleBuilder >> at: aPoint putBarLabel: aString value: aNumber max: maxNumber [
	| perten string width |

	width := 20.
	perten := ((aNumber / maxNumber) * width) asInteger.
	string := ('{1} {2}/{3}' format: { aString. aNumber. maxNumber }) padRightTo: width.
	
	self application defer: [ 
		self backgroundColor: Color green.
		self foregroundColor: Color white.
		self at: translation + aPoint putString: (string copyFrom: 1 to: perten).
		aNumber = maxNumber ifFalse: [ 
			self backgroundColor: Color red.
			self 
				at: (translation + aPoint) + (perten@0) 
				putString: (string copyFrom: (perten + 1) to: width) ] ]
]

{ #category : #api }
RgConsoleBuilder >> at: aPoint putCharacter: aCharacter [

	self application defer: [ 
		self terminal 
			saveCursor;
			cursorPosition: translation + aPoint;
			feedBytes: aCharacter asString utf8Encoded;
			restoreCursor ]
]

{ #category : #api }
RgConsoleBuilder >> at: aPoint putString: aString [

	self application defer: [ 
		self terminal
			saveCursor;
			cursorPosition: translation + aPoint;
			feedBytes: (aString utf8Encoded copyWith: 0);
			restoreCursor ]
]

{ #category : #api }
RgConsoleBuilder >> backgroundColor: bgColor [

	lastBackgroundColor = bgColor ifTrue: [ ^ self ].
	self terminal backgroundColor: bgColor.
	lastBackgroundColor := bgColor
]

{ #category : #api }
RgConsoleBuilder >> clearCharacterAt: aPoint [

	self 
		at: aPoint 
		putCharacter: Character space
]

{ #category : #api }
RgConsoleBuilder >> clearLine: aNumber [

	self application defer: [ 
		self terminal
			saveCursor;
			cursorPosition: 1@(translation y + aNumber);
			clearLine;
			restoreCursor ]
]

{ #category : #accessing }
RgConsoleBuilder >> console [

	^ console
]

{ #category : #api }
RgConsoleBuilder >> foregroundColor: fgColor [
	
	lastForegroundColor = fgColor ifTrue: [ ^ self ]. 
	self terminal foregroundColor: fgColor.
	lastForegroundColor := fgColor 
]

{ #category : #initialization }
RgConsoleBuilder >> initializeConsole: aConsole [

	self initialize.
	translation := 0@0.
	console := aConsole.

]

{ #category : #accessing }
RgConsoleBuilder >> terminal [

	^ self console terminal
]

{ #category : #api }
RgConsoleBuilder >> translateTo: aPoint [

	translation := aPoint
]

{ #category : #accessing }
RgConsoleBuilder >> translation [

	^ translation
]

{ #category : #api }
RgConsoleBuilder >> withForegroundColor: fgColor backgroundColor: bgColor do: aBlock [

	(fgColor isNotNil and: [ fgColor ~= lastForegroundColor ]) 
		ifTrue: [ self foregroundColor: fgColor ].
	(bgColor isNotNil and: [ bgColor ~= lastBackgroundColor ]) 
		ifTrue: [ self backgroundColor: bgColor ].
	
	aBlock value
]
