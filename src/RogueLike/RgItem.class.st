Class {
	#name : #RgItem,
	#superclass : #RgEntity,
	#category : #'RogueLike-Model-Items'
}

{ #category : #queries }
RgItem class >> allItems [

	^ self allSubclasses reject: [ :each | each isAbstract ] 
]

{ #category : #testing }
RgItem class >> isAbstract [

	^ self = RgItem
]

{ #category : #actions }
RgItem >> doActivate: anAction [

	self subclassResponsibility
]

{ #category : #testing }
RgItem >> isBlocking [

	^ false
]

{ #category : #testing }
RgItem >> isItem [

	^ true
]

{ #category : #testing }
RgItem >> isWalkable [

	^ true
]

{ #category : #'action factory' }
RgItem >> newAction: anActor [

	^ (RgActivateItemAction for: anActor)
		item: self;
		yourself
]
