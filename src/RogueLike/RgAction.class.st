Class {
	#name : #RgAction,
	#superclass : #Object,
	#instVars : [
		'console',
		'actor'
	],
	#category : #'RogueLike-Actions'
}

{ #category : #'instance creation' }
RgAction class >> for: anActor [

	^ self new actor: anActor
]

{ #category : #accessing }
RgAction >> actor [

	^ actor
]

{ #category : #accessing }
RgAction >> actor: anActor [

	actor := anActor
]

{ #category : #accessing }
RgAction >> addMessage: aString [

	self 
		addMessage: aString 
		color: self defaultColor
]

{ #category : #accessing }
RgAction >> addMessage: aString color: aColor [

	self console 
		addMessage: aString 
		color: aColor
]

{ #category : #accessing }
RgAction >> console [

	^ console
]

{ #category : #accessing }
RgAction >> console: anObject [

	console := anObject
]

{ #category : #accessing }
RgAction >> defaultColor [

	^ Color white
]

{ #category : #execution }
RgAction >> execute [

	self subclassResponsibility
]

{ #category : #execution }
RgAction >> executeOn: aConsole [

	self console: aConsole.
	self execute
]
