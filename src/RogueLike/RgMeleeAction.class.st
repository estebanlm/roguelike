Class {
	#name : #RgMeleeAction,
	#superclass : #RgBumpAction,
	#instVars : [
		'target'
	],
	#category : #'RogueLike-Actions'
}

{ #category : #'instance creation' }
RgMeleeAction class >> appliesTo: toTile [

	^ toTile isBlocked
]

{ #category : #'instance creation' }
RgMeleeAction class >> newActor: anActor tile: aTile [

	^ self new 
		actor: anActor;
		targetTile: aTile;
		yourself
]

{ #category : #execution }
RgMeleeAction >> execute [

	self actor doAttack: self
]

{ #category : #private }
RgMeleeAction >> obtainTarget [
	
	^ (targetTile enemiesOf: self actor) 
		ifNotEmpty: [ :enemies | enemies first ]
		ifEmpty: [ nil ]
]

{ #category : #execution }
RgMeleeAction >> reportDamageTaken: damage target: target [
	
	self
		addMessage: ('{1} takes {2} point{3} damage.' format: { 
			target name. 
			damage. damage > 1 ifTrue: [ 's' ] ifFalse: [ '' ]}) 
		color: (target isPlayer ifTrue: [ Color yellow ] ifFalse: [ Color white ])
]

{ #category : #execution }
RgMeleeAction >> reportTargetStatus: target [

	target isAlive ifTrue: [ ^ self ].
	
	self
		addMessage: ('{1} is dead.' format: { target name }) 
		color: Color red
]

{ #category : #accessing }
RgMeleeAction >> target [

	^ target ifNil: [ target := self obtainTarget ]
]

{ #category : #accessing }
RgMeleeAction >> target: anActor [

	target := anActor
]
