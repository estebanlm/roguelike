Class {
	#name : #RgMapGenerator,
	#superclass : #Object,
	#instVars : [
		'map'
	],
	#category : #'RogueLike-Generator'
}

{ #category : #adding }
RgMapGenerator >> addRoom: aRectangle [
	| rect |
	
	rect := aRectangle insetBy: 1.
	rect left to: rect right do: [ :x |
		rect top to: rect bottom do: [ :y |
			self map add: (RgFloor newPosition: x@y) ] ]
]

{ #category : #adding }
RgMapGenerator >> addTunnelFrom: start to: end [

	self map addAll: ((self tunnelFrom: start to: end) collect: [ :each | RgFloor newPosition: each ]) 
]

{ #category : #generating }
RgMapGenerator >> generate [

	self generateMap.
	self generatePlayer.
	self generateEntities.
	self generateItems.
	"obtain pathfinder"
	self map pathFinder
]

{ #category : #generating }
RgMapGenerator >> generateEntities [

]

{ #category : #generating }
RgMapGenerator >> generateItems [

]

{ #category : #generating }
RgMapGenerator >> generateMap [

	self subclassResponsibility
]

{ #category : #generating }
RgMapGenerator >> generatePlayer [

	self subclassResponsibility
]

{ #category : #accessing }
RgMapGenerator >> map [

	^ map
]

{ #category : #accessing }
RgMapGenerator >> map: aMap [ 
	
	map := aMap
]

{ #category : #accessing }
RgMapGenerator >> player [

	^ self map player
]

{ #category : #private }
RgMapGenerator >> random: aNumberOrCollection [

	self subclassResponsibility
]

{ #category : #private }
RgMapGenerator >> tunnelFrom: start to: end [
	| corner |
		
	"50% chance"
	corner := (self random: 100) <= 50
		ifTrue: [ 
			"Move horizontally, then vertically"
			(end x) @ (start y) ]
		ifFalse: [ 
			"Move vertically, then horizontally"
			(start x) @ (end y) ].
	
	^ (RgUtils bresenhamFrom: start to: corner), (RgUtils bresenhamFrom: corner to: end)
]
