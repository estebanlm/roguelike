Class {
	#name : #RgActor,
	#superclass : #RgEntity,
	#instVars : [
		'hitPoints'
	],
	#category : #'RogueLike-Model-Actors'
}

{ #category : #actions }
RgActor >> damageBy: aNumber [

	hitPoints := (hitPoints - aNumber) max: 0
]

{ #category : #accessing }
RgActor >> defense [

	^ self subclassResponsibility
]

{ #category : #actions }
RgActor >> doAttack: anAction [
	| damage target |

	target := anAction target.
	damage := self power - target defense.
	damage > 0 ifTrue: [ 
		 target damageBy: damage ].
	anAction reportDamageTaken: damage target: target.
	anAction reportTargetStatus: target.
]

{ #category : #actions }
RgActor >> doMove: anAction [

	self moveTo: anAction targetTile
]

{ #category : #actions }
RgActor >> heal: aNumber [
	"Answer amount healed"
	| oldHitPoints |

	self hitPoints = self maxHitPoints ifTrue: [ ^ 0 ].

	oldHitPoints := self hitPoints.
	self hitPoints: self hitPoints + aNumber.
	
	^ self hitPoints - oldHitPoints
]

{ #category : #accessing }
RgActor >> hitPoints [

	^ hitPoints
]

{ #category : #accessing }
RgActor >> hitPoints: aNumber [

	hitPoints := 0 max: (aNumber min: self maxHitPoints)
]

{ #category : #initialization }
RgActor >> initialize [

	super initialize.
	hitPoints := self maxHitPoints
]

{ #category : #testing }
RgActor >> isActor [

	^ true
]

{ #category : #testing }
RgActor >> isAlive [

	^ self hitPoints > 0
]

{ #category : #testing }
RgActor >> isBlocking [

	^ true
]

{ #category : #testing }
RgActor >> isHostileTo: anActor [

	^ false
]

{ #category : #testing }
RgActor >> isWalkable [

	^ false
]

{ #category : #accessing }
RgActor >> maxHitPoints [

	^ self subclassResponsibility
]

{ #category : #actions }
RgActor >> moveTo: aTile [ 

	self tile ifNotNil: [ self tile removeEntity: self ].
	aTile addEntity: self
]

{ #category : #'as yet unclassified' }
RgActor >> movementToPlayer [ 
	| path |

	path := self pathTo: self map player tile.
	^ path first
]

{ #category : #'accessing pathfind' }
RgActor >> pathTo: aTile [
	| pathFinder cost |
	
	pathFinder := self map pathFinder.
	pathFinder reset.
	
	cost := pathFinder 
		runFrom: self tile 
		to: aTile.
	
	"First is the mobile position and I am not interested on it usually"
	^ cost ~= Float infinity
		ifTrue: [ pathFinder backtrack allButFirst  ]
		ifFalse: [ #() ]
]

{ #category : #'accessing pathfind' }
RgActor >> pathToPosition: aPoint [

	^ self pathTo: (self map tileAt: aPoint)
]

{ #category : #'as yet unclassified' }
RgActor >> playTurn [
	| delta distance |

	"in line of sight of player"
	self tile isVisible ifFalse: [ ^ RgNoAction for: self ].

	delta := self map player position - self position. 
	distance := delta x abs max: delta y abs. "Chebyshev distance"
	
	^ (distance <= 1 
		ifTrue: [ RgMeleeAction newActor: self tile: self map player tile ]
		ifFalse: [ RgMovementAction newActor: self tile: self movementToPlayer ])
]

{ #category : #accessing }
RgActor >> power [

	^ self subclassResponsibility
]

{ #category : #actions }
RgActor >> rest: aNumber [

	self hitPoints: self hitPoints + aNumber
]
