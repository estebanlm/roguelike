Class {
	#name : #RgPriorityElement,
	#superclass : #Object,
	#instVars : [
		'element',
		'priority'
	],
	#category : #'RogueLike-Utils'
}

{ #category : #accessing }
RgPriorityElement >> element [

	^ element
]

{ #category : #accessing }
RgPriorityElement >> element: anObject [

	element := anObject
]

{ #category : #accessing }
RgPriorityElement >> priority [

	^ priority
]

{ #category : #accessing }
RgPriorityElement >> priority: anObject [

	priority := anObject
]
