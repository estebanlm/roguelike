Class {
	#name : #RgHealingConsumable,
	#superclass : #RgConsumable,
	#instVars : [
		'amount'
	],
	#category : #'RogueLike-Model-Items'
}

{ #category : #testing }
RgHealingConsumable class >> isAbstract [

	^ super isAbstract or: [ self = RgHealingConsumable ]
]

{ #category : #'instance creation' }
RgHealingConsumable class >> newAmount: aNumber [

	^ self new 
		amount: aNumber;
		yourself
]

{ #category : #accessing }
RgHealingConsumable >> amount [

	^ amount
]

{ #category : #accessing }
RgHealingConsumable >> amount: anObject [

	amount := anObject
]

{ #category : #actions }
RgHealingConsumable >> doActivate: anAction [
	| consumer amountRecovered |

	consumer := anAction actor.
	amountRecovered := consumer heal: self amount.
	self amount: (self amount - amountRecovered).
	
	amountRecovered > 0 
		ifTrue: [ 
			anAction 
				addMessage: ('You consume {1}, and recover {2} hit points.' 
					format: { self name. amountRecovered }) 
				color: Color green  ]
		ifFalse: [
			RgImpossible signal: 'Your health is already full.' ]
]

{ #category : #testing }
RgHealingConsumable >> isConsumed [

	^ self amount = 0
]
