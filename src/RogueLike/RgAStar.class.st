"
Implements A* algorithm as described here: 

https://en.wikipedia.org/wiki/A*_search_algorithm

But, basically what I did is to adapt what is explained here: 

https://www.redblobgames.com/pathfinding/a-star/implementation.html

IMPORTANT: This algorithm assumes nodes and edges will be expresed on instances of `RgTile`.
"
Class {
	#name : #RgAStar,
	#superclass : #MalDijkstra,
	#instVars : [
		'cardinalWeight',
		'diagonalWeight',
		'needsReset'
	],
	#category : #'RogueLike-Utils'
}

{ #category : #example }
RgAStar class >> example [
	<sampleInstance>
	| nodes edges |

	nodes := { 
		1@1. 2@1. 3@1. 4@1. 5@1.
		1@2. 2@2. 3@2. 4@2. 5@2.
	 }.
	edges := { 
		1@1->{"2@1." 1@2}. 
		2@1->{3@1. 2@2}. 
		3@1->{"4@1." 3@2}. 
		4@1->{5@1. 4@2}. 
		5@1->{5@2}.
		1@2->{1@1. 2@2}. 
		2@2->{2@1. "3@2"}. 
		3@2->{3@1. 4@2}. 
		4@2->{4@1. 5@2}. 
		5@2->{5@1}.
	}.

	^ MalDijkstra new 
		nodes: nodes;
		edges: edges from: #key toAll: #value;
		runFrom: (1@1) to: (5@1);
		backtrack
]

{ #category : #backtracking }
RgAStar >> backtrack: fromNode [
	| result |

	result := OrderedCollection new.
	self backtrack: fromNode in: result.
	^ (result collect: [ :each | each model ]) reversed
]

{ #category : #backtracking }
RgAStar >> backtrack: fromNode in: result [
	| previousNode |
	
	fromNode pathWeight = Float infinity
		ifTrue: [ ^ self ]. 
	
	fromNode = self start ifTrue: [ 
		result add: fromNode.
		^ self ].
		
	previousNode := (fromNode previousNodes 
		reject: [ :each | result includes: each ])
		detectMin: [ :n | n pathWeight ].
	result add: fromNode.
	previousNode ifNil: [ ^ self ].
	self backtrack: previousNode in: result
]

{ #category : #accessing }
RgAStar >> cardinalWeight [

	^ cardinalWeight
]

{ #category : #accessing }
RgAStar >> cardinalWeight: aNumber [

	cardinalWeight := aNumber
]

{ #category : #accessing }
RgAStar >> diagonalWeight [

	^ diagonalWeight
]

{ #category : #accessing }
RgAStar >> diagonalWeight: aNumber [

	diagonalWeight := aNumber
]

{ #category : #running }
RgAStar >> edgeRelationWeightA: aTile b: bTile [

	^ (aTile position x = bTile position x or: [ aTile position y = bTile position y ])
		ifTrue: [ self cardinalWeight ]
		ifFalse: [ self diagonalWeight ]
]

{ #category : #'building - graph' }
RgAStar >> edges: aCollection from: source toAll: targets [

	self ensureNodesAreSorted.
	super 
		edges: aCollection 
		from: source 
		toAll: targets
]

{ #category : #accessing }
RgAStar >> findNode: aModel ifAbsent: aBlock [
	
	self canUseSortedNodes ifFalse: [ 
		^ self nodes 
			detect: [ :node | node model = aModel ] 
			ifNone: [ aBlock value ] ].

	^ self nodes 
		findBinary: (self findBinaryBlock: aModel) 
		ifNone: [ aBlock value ]
]

{ #category : #running }
RgAStar >> heuristicA: aTile b: bTile [
	
	^ (aTile position x - bTile position x) abs 
		+ (aTile position y - bTile position y) abs
]

{ #category : #initialization }
RgAStar >> initialize [

	super initialize.
	needsReset := false.
	cardinalWeight := 0.
	diagonalWeight := 0
]

{ #category : #initialization }
RgAStar >> reset [

	needsReset ifFalse: [ ^ self ].
	super reset.
	needsReset := false
]

{ #category : #running }
RgAStar >> runFrom: startModel to: endModel [

	needsReset := true.
	^ super runFrom: startModel to: endModel
]

{ #category : #running }
RgAStar >> traverse: node [
	| frontier |
	
	frontier := RgPriorityQueue new.
	frontier at: 0 put: node.

	[ frontier isEmpty ] 
	whileFalse: [ | current |
		current := frontier get.
		"early exit"
		current = self end ifTrue: [ ^ self ].
		current nextEdges 
			reject: [ :each | each isVisited ]
			thenDo: [ :edge | | priority weight |
				weight := edge to pathWeight min: (current pathWeight + edge weight).
				(edge to ~= self start and: [ edge to ~= self end ])
					ifTrue: [  weight := weight + edge to model pathWeight ].
				"weight := weight + (self edgeRelationWeightA: current model b: edge to model)."
				edge to pathWeight: weight.
				edge visited: true.
				priority := weight + (self heuristicA: current model b: self end model).
				frontier at: priority put: edge to.
			]
	]
]
