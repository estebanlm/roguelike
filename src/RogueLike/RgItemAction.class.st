Class {
	#name : #RgItemAction,
	#superclass : #RgAction,
	#instVars : [
		'item'
	],
	#category : #'RogueLike-Actions'
}

{ #category : #accessing }
RgItemAction >> item [

	^ item
]

{ #category : #accessing }
RgItemAction >> item: anObject [

	item := anObject
]
