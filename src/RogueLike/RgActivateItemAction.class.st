Class {
	#name : #RgActivateItemAction,
	#superclass : #RgItemAction,
	#category : #'RogueLike-Actions'
}

{ #category : #execution }
RgActivateItemAction >> execute [

	self item doActivate: self
]
