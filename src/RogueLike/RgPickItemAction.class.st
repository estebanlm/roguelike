Class {
	#name : #RgPickItemAction,
	#superclass : #RgBumpAction,
	#instVars : [
		'item'
	],
	#category : #'RogueLike-Actions'
}

{ #category : #'instance creation' }
RgPickItemAction class >> appliesTo: toTile [
	
	^ toTile items notEmpty
]

{ #category : #execution }
RgPickItemAction >> execute [

	self actor 
		doMove: self;
		doPickItem: self
]

{ #category : #accessing }
RgPickItemAction >> item [ 

	^ item ifNil: [ item := self obtainItem ]
]

{ #category : #accessing }
RgPickItemAction >> item: anItem [

	item := anItem
]

{ #category : #accessing }
RgPickItemAction >> obtainItem [
	
	^ targetTile items
		ifNotEmpty: [ :items | items first ]
		ifEmpty: [ nil ]
]
