"
Implements lehmer randomizer algorithm,
Not sure is usable since this is less efficient that what you get on `Random`, but one never knows :)
"
Class {
	#name : #LehmerRandom,
	#superclass : #Object,
	#instVars : [
		'seed'
	],
	#category : #LehmerRandom
}

{ #category : #'instance creation' }
LehmerRandom class >> seed: aNumber [

	^ self new seed: aNumber
]

{ #category : #accessing }
LehmerRandom >> nextCollection: aCollection [
	
	aCollection size = 1 ifTrue: [ ^ aCollection first ].
	^ aCollection at: (self 
		nextIntegerMin: 1 
		max: aCollection size + 1)
]

{ #category : #private }
LehmerRandom >> nextGeneratedNumber [
	| tmp |

	seed := (seed + 16rE120FC15) bitAnd: 16rFFFFFFFFFF.
	tmp := (seed * 16r4A39B70D) bitAnd: 16rFFFFFFFFFF.
	tmp:= ((tmp bitShiftMagnitude: 32) bitXor: tmp) bitAnd: 16rFFFFFFFFFF.
	tmp := (tmp * 16r12FAD5C9) bitAnd: 16rFFFFFFFFFF.
	
	^ ((tmp bitShiftMagnitude: 32) bitXor: tmp) bitAnd: 16rFFFFFFFFFF
]

{ #category : #accessing }
LehmerRandom >> nextInteger: max [
	
	max = 1 ifTrue: [ ^ 1 ].
	^ self nextIntegerMin: 1 max: max
]

{ #category : #accessing }
LehmerRandom >> nextIntegerMin: min max: max [ 

	^ (self nextGeneratedNumber % (max - min)) + min
]

{ #category : #accessing }
LehmerRandom >> seed: aNumber [ 

	seed := aNumber
]
