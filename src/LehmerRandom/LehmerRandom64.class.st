"
64 bit version from the lehmer algorithm.
Taken from here: https://lemire.me/blog/2019/03/19/the-fastest-conventional-random-number-generator-that-can-pass-big-crush/
"
Class {
	#name : #LehmerRandom64,
	#superclass : #LehmerRandom,
	#category : #LehmerRandom
}

{ #category : #private }
LehmerRandom64 >> nextGeneratedNumber [
	| tmp | 
 	
	seed := seed + 16r60bee2bee120fc15.
  	tmp := seed * 16ra3b195354a39b70d.
	tmp := (tmp bitShiftMagnitude: 64) bitXor: tmp.
  	tmp := tmp * 16r1b03738712fad5c9.

	^ (tmp bitShiftMagnitude: 64) bitXor: tmp
]
