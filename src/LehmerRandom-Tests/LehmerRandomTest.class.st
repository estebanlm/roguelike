Class {
	#name : #LehmerRandomTest,
	#superclass : #ParametrizedTestCase,
	#instVars : [
		'randomClass'
	],
	#category : #'LehmerRandom-Tests'
}

{ #category : #tests }
LehmerRandomTest class >> testParameters [

	^ ParametrizedTestMatrix new
		addCase: { #randomClass -> LehmerRandom };
		addCase: { #randomClass -> LehmerRandom64 };
		yourself.
]

{ #category : #accessing }
LehmerRandomTest >> randomClass: aClass [

	randomClass := aClass
]

{ #category : #tests }
LehmerRandomTest >> testAtRandomOfOneValuesDoNotAnswerAlwaysSameOutput [
	| random answers |

	random := randomClass seed: 42.

	answers := Set new.
	1000 timesRepeat: [ 
		answers add: (random nextCollection: #(A)) ].
		
	self assert: answers size equals: 1
]

{ #category : #tests }
LehmerRandomTest >> testAtRandomOfSmallCollectionValues [
	| random collection answers |

	10 to: 2 by: -1 do: [ :index | 
		random := randomClass seed: 42.
		collection := #(A B C D E F G H I J) first: index.
		answers := Set new.
		1000 timesRepeat: [ 
			answers add: (random nextCollection: collection) ].
		self assert: answers size equals: index.
		self assertCollection: answers hasSameElements: collection ]
]

{ #category : #tests }
LehmerRandomTest >> testNextGeneratedNumber [
	| answers random num |

	num := 100000.
	random := randomClass seed: 42.
	answers := Set new: num.
	1 to: num do: [ :index |
		answers add: random nextGeneratedNumber ].
	self assert: answers size equals: num
]
