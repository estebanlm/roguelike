Class {
	#name : #BaselineOfProceduralGalaxy,
	#superclass : #BaselineOf,
	#category : #BaselineOfProceduralGalaxy
}

{ #category : #baselines }
BaselineOfProceduralGalaxy >> baseline: spec [
	<baseline>
	
	spec for: #common do: [ 
		self stargate: spec.
		spec 
			package: 'LehmerRandom';
			package: 'ProceduralGalaxy' with: [ spec requires: 'Stargate' ] ]
]

{ #category : #'external projects' }
BaselineOfProceduralGalaxy >> stargate: spec [

	spec 
		baseline: 'Stargate' 
		with: [ spec repository: 'github://estebanlm/stargate/src' ]
]
